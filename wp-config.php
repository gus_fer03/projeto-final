<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rWkJsPNtZJqRBHOFb/d2I6O8x1N+GhkjB2FLRMu/CYjxR40hPBq0RyQk9vXKHbcrdX1NyTSaazgc53Xwfpj2EA==');
define('SECURE_AUTH_KEY',  'UUyE5TRUQ47nOOt1GuK05IcXoLO/fjk2YhtPb0vjTUvHN7poBirtyD93KP8VyuGdCh9GD18/UgW12/ONoxvrhQ==');
define('LOGGED_IN_KEY',    't1TXZOVlgyFH+KBh1+3/XUZWbe6jNfNx8nlrt6MSPUKF9k/EBcLpssKLzIdxkq2XrJJWTV3sTs1HvpCQz/K3zA==');
define('NONCE_KEY',        'h17etlIxDJVdbyeUD5S1pfX36GHlZa+H41rOlhO64QTFRFO3jA/IoLS3um7EKdaPoon+xzm8jQUpw9defKO0mA==');
define('AUTH_SALT',        'Lt7yZZo8sNPC17i4/XQQrDgVqkSawjAXSNn1IC1fMLNQIh3HA4j2Qdr8lypkp8F6KwmuHr8hMEJHZru9JoP7LQ==');
define('SECURE_AUTH_SALT', 'q4fL2k341FR6ySI/6MHPcdxCVB6ybledY9dA/T32jTA+8XJFznq1XWvYUjA2quEXHTlAHBHX5v99M2ZlLWHfHQ==');
define('LOGGED_IN_SALT',   'Y1ZUOVlwMWobpD0yaVtxqPhN72UkW5flvG3OF65uew435EbTSlwZ4QZ/zpS+GFEsBNhQia/pNbdBvkjTQLWKIg==');
define('NONCE_SALT',       '1B4mf2JsJ6KUbtV58IEBTOJmxCQmlwV/fGVd8Igx1ORnrSjizYNpNmU0+OGnJO4aB6NteNgSguqeiD30cY8jSg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
