// Update price according to variable price
window.addEventListener('DOMContentLoaded', (event) => {
    document.querySelector("button.minus").addEventListener("click", (event) => {

        const qty = Number(document.querySelector("input.qty").value)
        let priceElement = document.querySelector(".wrap-top-price").querySelector("bdi")

        let price = priceElement.innerText.replace(",", ".").split(" ")[1]
        const newPrice = qty * parseFloat(price)

        priceElement.innerText = priceElement.innerText.replace(price, newPrice)
    })
})