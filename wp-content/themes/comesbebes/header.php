<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php bloginfo("name"); ?> <?php wp_title(" | "); ?> </title>
    <?php wp_head();?>
</head>
<body>

    <div class="sidebar-container" id="sidebar-container">
        <section class="sidebar-outside" id="sidebar-outside"></section>
        <section class="sidebar" id="sidebar">
            <div class="sidebar-divs-container" id="sidebar-divs-container">
                <div class="sidebar-div-1">
                    <h1 class="sidebar-titulo">CARRINHO</h1>
                    <hr>
                </div>
                <div class="sidebar-carrinho">
                    <?php $items = WC()->cart->get_cart();
                        foreach($items as $item) {
                            $product_id = strval($item['product_id']);
                            $product_name = $item['data']->get_title();
                            $quantity = $item['quantity'];
                            $price = ($item['data']->get_price())*$quantity;
                            
                            $product = new WC_product($product_id);
                            $img_id = $product->get_image_id();
                            $img_url = wp_get_attachment_image_url( $img_id, 'full' );

                            echo "<div class='carrinho-item-container carrinho-item-" . $product_id . "'>";
                                echo "<div class='carrinho-container-img' style='background-image: url(" . $img_url . ")'></div>";

                                echo "<div class='carrinho-container-txt'>";
                                    echo "<h2 class='carrinho-nome'>" . $product_name . "</h2>";
                                    echo "<div class='carrinho-container-flex'>";
                                        echo "<div class='carrinho-botoes-quantidade'>";
                                            echo "<button class='carrinho-botao-quantidade'>-</button>";
                                            echo "<input type='number' value='$quantity' class='carrinho-quantidade'>";
                                            echo "<button class='carrinho-botao-quantidade'>+</button>";
                                        echo "</div>";
                                        echo "<p class='carrinho-preco'>R$" . number_format($price,2) . "</p>";
                                    echo "</div>";
                                echo "</div>";
                            echo "</div>";
                        };
                    ?>
                    <hr>
                </div>

                <div class="sidebar-div-2">
                    <p>Total do carrinho: R$ <?php echo WC()->cart->total; ?></p>
                </div>
                <div class="sidebar-button-container">
                    <button onclick="window.location = `/checkout`" class="botao-comprar">COMPRAR</button>
                </div>
            </div>
        </section>
    </div>

    <header class="header-container">
            <div class="navbar">
                <div class="header-left">
                    <div class="container-image"><a href="/home"><img class="img-logo" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png"></a></div>
                    <div class="container-search"><?php get_search_form(); ?></div>
                </div>
                
                <nav class="header-right">
                    <ul class="nav-items">
                        <li class="nav-item"><a href="/loja/"><button class="nav-fazerpedido">Faça um pedido</button></a></li>
                        <li class="nav-item" id="botao-carrinho"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icone-cart.png"></li>
                        <li class="nav-item" id="botao-perfil"><a href="/minha-conta/"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icone-user.png"></a></li>
                    </ul>
                </nav>
            </div>
    </header>

