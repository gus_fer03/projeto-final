<?php

    add_action('wp_enqueue_scripts', 'carregarCSS');
    add_action('wp_enqueue_scripts', 'carregarJS');

    function carregarCSS(){
        wp_enqueue_style( 'ps2021.3-reset-style', get_template_directory_uri() . '/css/reset.css');
        wp_enqueue_style( 'style.css', get_template_directory_uri().'/style.css');
    }

    function carregarJS(){
        wp_enqueue_script('showsidebar', get_template_directory_uri() . '/js/showsidebar.js');
        wp_enqueue_script('update-cart', get_template_directory_uri() . '/js/update-cart.js');
        //wp_enqueue_script('variation-price', get_template_directory_uri() . '/js/variation-price.js');
    }

    function mytheme_add_woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
    add_filter( 'woocommerce_get_related_product_tag_terms', function( $term_ids, $product_id ){
        return array();
    }, 10, 2 );


    add_action( 'init', 'bc_remove_wc_breadcrumbs' );
    function bc_remove_wc_breadcrumbs() {
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
    }

?>

<?php // Função de criar os cards dos produtos


    remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash');

    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
    function action_woocommerce_template_loop_price() {
        global $product;
        echo "<p>R$ ";
        if ($product->is_type( 'variable' )) {
            #1 Get product variations
            $product_variations = $product->get_available_variations();
            
            #2 Get one variation id of a product
            $variation_product_id_max = end($product_variations)['variation_id'];
            $variation_product_id_min = $product_variations [0]['variation_id'];

            #3 Create the product object
            $variation_product_max = new WC_Product_Variation( $variation_product_id_max );
            $variation_product_min = new WC_Product_Variation( $variation_product_id_min );

            #4 Use the variation product object to get the variation prices
            echo $variation_product_max ->regular_price;
            echo $variation_product_max ->sale_price;
            echo " - ";
            echo $variation_product_min ->regular_price;
            echo $variation_product_min ->sale_price;
        }

        else
            {if( $product->is_on_sale() ) {
                echo "<strike>" . $product->get_regular_price() . "</strike> &rarr; ";
                echo $product->get_sale_price();
            }
            else {echo $product->get_regular_price();}}

        echo "  </p><button onclick='window.location = `?add-to-cart={$product->id}`'><img src='" . get_stylesheet_directory_uri() . "/img/icone-buy.png'></button>";
    }
    add_action('woocommerce_after_shop_loop_item_title', 'action_woocommerce_template_loop_price', 10 );


    remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
    function action_woocommerce_shop_loop_item_title() {
        $output =   "   <div class='categoria-label'>
                            <p>" . get_the_title() . "</p>
                            <div class='container-preco-icon'>";

        echo $output; 
    }
    add_action( 'woocommerce_shop_loop_item_title', 'action_woocommerce_shop_loop_item_title', 10 );

    /**
     * Edit default Woocommerce product loop thumbnail template
     * As there is no dedicated Woocommerce template (eg wp-content/plugins/woocommerce/templates/loop/price.php)
     * because it's generated using filter, we must remove Woocommerce hook, and add our own "at the same place"
     * to edit the product loop thumbnail template
     * tested up to (12/10/2020) : 
     * Wordpress 5.7
     * Woocommerce 3.8.1
     * PHP 7.3.7
     * Sage 9.0.9
     * source: https://gist.github.com/krogsgard/3015581
     * HOW TO USE: add in active theme functions.php file
     */

    /**
     * Remove woocommerce hooked action (method woocommerce_template_loop_product_thumbnail on woocommerce_before_shop_loop_item_title
     * hook
     */
    remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
    /**
     * Add our own action to the woocommerce_before_shop_loop_item_title hook with the same priority that woocommerce used
     */
    add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

    //remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);

    /**
     * WooCommerce Loop Product Thumbs
     */
    if (!function_exists('woocommerce_template_loop_product_thumbnail')) {
        /**
         * echo thumbnail HTML
         */
        function woocommerce_template_loop_product_thumbnail()
        {
            echo woocommerce_get_product_thumbnail();
        }
    }

    /**
     * WooCommerce Product Thumbnail
     */
    if (!function_exists('woocommerce_get_product_thumbnail')) {

        /**
         * @param string $size
         * @param int $placeholder_width
         * @param int $placeholder_height
         * @return string
         */
        function woocommerce_get_product_thumbnail($size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0)
        {
            global $post, $woocommerce;
            
            //NOTE: those are PHP 7 ternary operators. Change to classic if/else if you need PHP 5.x support.
            $placeholder_width = !$placeholder_width ?
                wc_get_image_size('shop_catalog_image_width')[ 'width' ] :
                $placeholder_width;

            $placeholder_height = !$placeholder_height ?
                wc_get_image_size('shop_catalog_image_height')[ 'height' ] :
                $placeholder_height;

            /**
             * EDITED HERE: here I added a div around the <img> that will be generated
             */

            /**
             * This outputs the <img> or placeholder image. 
             * it's a lot better to use get_the_post_thumbnail() that hardcoding a text <img> tag
             * as wordpress wil add many classes, srcset and stuff.
             */
            $output .= "<div class='container-secao-2-item-template produto' style='background-image: url(" . get_the_post_thumbnail_url() . ");'>";

            /**
             * Close added div .my_new_wrapper
             */

            return $output;
        }}
?>

<?php 

    // 1. Show plus minus buttons
    
    add_action( 'woocommerce_after_quantity_input_field', 'silva_display_quantity_plus' );
    
    function silva_display_quantity_plus() {
    echo '<button type="button" class="plus" >+</button>';
    }
    
    add_action( 'woocommerce_before_quantity_input_field', 'silva_display_quantity_minus' );
    
    function silva_display_quantity_minus() {
    echo '<button type="button" class="minus" >-</button>';
    }
    
    // -------------
    // 2. Trigger update quantity script
    
    add_action( 'wp_footer', 'silva_add_cart_quantity_plus_minus' );
    
    function silva_add_cart_quantity_plus_minus() {
    
    if ( ! is_product() && ! is_cart() ) return;
        
    wc_enqueue_js( "   
            
        $('form.cart,form.woocommerce-cart-form').on( 'click', 'button.plus, button.minus', function() {
    
            var qty = $( this ).parent( '.quantity' ).find( '.qty' );
            var val = parseFloat(qty.val());
            var max = parseFloat(qty.attr( 'max' ));
            var min = parseFloat(qty.attr( 'min' ));
            var step = parseFloat(qty.attr( 'step' ));
    
            if ( $( this ).is( '.plus' ) ) {
                if ( max && ( max <= val ) ) {
                qty.val( max );
                } else {
                qty.val( val + step );
                }
            } else {
                if ( min && ( min >= val ) ) {
                qty.val( min );
                } else if ( val > 1 ) {
                qty.val( val - step );
                }
            }
    
        });
            
    " );
    }

?>

<?php // SINGLE PRODUCT

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash');
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');
    
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price');
    
    // Mostra o preço abaixo de tudo se o produto nao for variavel
    function woocommerce_template_single_price() {
        global $product;
        if (!$product->is_type( 'variable' )){
            wc_get_template( 'single-product/price.php' );
        }
    }
    add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 100);

    // Mostrar descrição
    add_action('woocommerce_single_product_summary', 'mostrarDesc', 6);
    function mostrarDesc() {
        echo "<div class='container-descricao-single-product'><p>" . get_the_excerpt() . "</p></div>";
    }

    // Mudar texto do botão de adicionar ao carrinho
    add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_add_to_cart_text');
 
    function woocommerce_custom_add_to_cart_text() {
    return __('Adicionar', 'woocommerce');
    }


    // ###################################################
    // Preço de variações 

    add_action('wp_footer', 'update_variable_product_price', 999, 0);
    function update_variable_product_price() {
        if( ! is_product() ) return;
        ?>
        <script>
            jQuery(function ($) {
                $(document).ready(function() {
                    var original_price_html = $('.wrap-top-price').html();
                    $('.entry-summary').on('click', '.reset_variations', function() {
                        $('.wrap-top-price').html(original_price_html);
                    });
                    $('.variations_form').on('woocommerce_variation_has_changed', function () {
                        if($('.woocommerce-variation-price .woocommerce-Price-amount').html().length) {
                            $('.wrap-top-price').empty();
                            $('.wrap-top-price').html($('.woocommerce-variation-price .woocommerce-Price-amount').html());
                        }
                    });
                })
            });
        </script>
        <?php
    }
    
?>

<?php // Carrinho sidebar 



?>