<div class="searchbar-wrapper">
    <form action="<?php echo bloginfo('url'); ?>/loja/" method="get">
        
        <div class="search-container">
            <button type="submit" id="searchbutton"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icone-search.png"></button>
            <input class ="searchtext" type="text" name="s" id="s" value="<?php echo the_search_query( ) ?>" placeholder="Buscar">
            <input type="text" name="post-type" value="product" class="hidden">
        </div>

    </form>
</div>