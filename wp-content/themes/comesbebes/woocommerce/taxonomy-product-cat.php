<?php get_header(); ?>

<div class="home-section-2">
    <div class="container-secao-2-items">
        <div class="container-secao-2-categorias">
            <h3 class="container-categorias-titulo">SELECIONE UMA CATEGORIA</h3>
            <ul class="secao-2-lista">
                <?php            
                    $taxonomy     = 'product_cat';
                    $orderby      = 'name';  
                    $show_count   = 0;      // 1 for yes, 0 for no
                    $pad_counts   = 0;      // 1 for yes, 0 for no
                    $hierarchical = 1;      // 1 for yes, 0 for no  
                    $title        = '';  
                    $empty        = 0;

                    $args = array(
                            'taxonomy'     => $taxonomy,
                            'orderby'      => $orderby,
                            'show_count'   => $show_count,
                            'pad_counts'   => $pad_counts,
                            'hierarchical' => $hierarchical,
                            'title_li'     => $title,
                            'hide_empty'   => $empty
                    );
                    $all_categories = get_categories( $args );
                    foreach ($all_categories as $cat) {
                        if($cat->category_parent == 0) {
                            $category_id = $cat->term_id; 
                            $category_name = $cat->name;
                            $thumbnail_id = get_woocommerce_term_meta($category_id, 'thumbnail_id', true);
                            $image = wp_get_attachment_url($thumbnail_id);
                            $link = get_term_link( $category_id, `product_cat` );

                            if($cat->slug != "uncategorized"){
                                echo "<li>";
                                echo    "<a href='{$link}'><div class='container-secao-2-item-template' style=" . "background-image:url('{$image}');" . ">";
                                echo        "<div><p class='categoria-label'>${category_name}</p></div>";
                                echo        "</div></a>";
                                echo "</li>";
                            }
                        }       
                    } 
                ?>
            </ul>
        </div>
    </div>
    <div class="container-secao-filtro">
        <div>
            <div class="secao-filtros">
                <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                <h2>PRATOS</h2>
                <p class="woocommerce-products-header__title page-title">COMIDA
                    <?php echo $wp_query->get_queried_object()->name; ?>
                </p>
                <?php endif; ?>
                <div class="filtros-pratos">
                    <div class="pratos-buscar">
                        <p>Buscar por nome:</p>
                        <div>
                            <form action=<?php
                                    echo bloginfo('url') . '/product-category/' . $wp_query->get_queried_object()->slug; ;
                                    ?> method="get">
                                <div class="search-categoria-produto">
                                    <button type="submit" id="searchbutton" class="hidden"></button>
                                    <input class="search-categoria-produto-text" type="text" name="s" id="s"
                                        value="<?php echo the_search_query( ); ?>"
                                        placeholder="">
                                    <input type="text" name="post-type" value="product" class="hidden">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="container-ordenar-preco">
                        <div class="pratos-ordenar">
                            <p>Ordenar por:</p>
                            <div>
                                <form class="woocommerce-ordering" method="get">
                                    <?php
                                                if ( ! wc_get_loop_prop( 'is_paginated' ) || ! woocommerce_products_will_display() ) {
                                                    return;
                                                }
                                                $show_default_orderby    = 'menu_order' === apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby', 'menu_order' ) );
                                                $catalog_orderby_options = apply_filters(
                                                    'woocommerce_catalog_orderby',
                                                    array(
                                                    'menu_order' => __( '', 'woocommerce' ),
                                                    'popularity' => __( 'Ordenar por popularidade', 'woocommerce' ),
                                                    'rating'     => __( 'Ordenar por avaliação', 'woocommerce' ),
                                                    'date'       => __( 'Ordenar por data', 'woocommerce' ),
                                                    'price'      => __( 'Ordenar por preço: decrescente', 'woocommerce' ),
                                                    'price-desc' => __( 'Ordenar por preço: crescente', 'woocommerce' ),
                                                    )
                                                );
                                                $default_orderby = wc_get_loop_prop( 'is_search' ) ? 'relevance' : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby', '' ) );
                                                // phpcs:disable WordPress.Security.NonceVerification.Recommended
                                                $orderby = isset( $_GET['orderby'] ) ? wc_clean( wp_unslash( $_GET['orderby'] ) ) : $default_orderby;
                                                // phpcs:enable WordPress.Security.NonceVerification.Recommended
                                                if ( wc_get_loop_prop( 'is_search' ) ) {
                                                    $catalog_orderby_options = array_merge( array( 'relevance' => __( 'Relevância', 'woocommerce' ) ), $catalog_orderby_options );
                                                    unset( $catalog_orderby_options['menu_order'] );
                                                }
                                                if ( ! $show_default_orderby ) {
                                                    unset( $catalog_orderby_options['menu_order'] );
                                                }
                                                if ( ! wc_review_ratings_enabled() ) {
                                                    unset( $catalog_orderby_options['rating'] );
                                                }
                                                if ( ! array_key_exists( $orderby, $catalog_orderby_options ) ) {
                                                    $orderby = current( array_keys( $catalog_orderby_options ) );
                                                }
                                                wc_get_template(
                                                    'loop/orderby.php',
                                                    array(
                                                    'catalog_orderby_options' => $catalog_orderby_options,
                                                    'orderby'                 => $orderby,
                                                    'show_default_orderby'    => $show_default_orderby,
                                                    )
                                                );
                                            ?>
                                </form>
                            </div>
                        </div>
                        <div class="pratos-filtro-preco">
                            <p>Filtro de preço:</p>
                            <div>
                                <?php defined( 'ABSPATH' ) || exit;?>
                                <?php do_action( 'woocommerce_widget_price_filter_start', $args ); ?>
                                <form method="get" action="<?php echo esc_url( $form_action ); ?>">
                                    <div class="price_slider_wrapper">
                                        <div class="price_slider" style="display:none;"></div>
                                        <div class="price_slider_amount" data-step="<?php echo esc_attr( $step ); ?>">
                                            <label for="min_price">De:</label>
                                            <input type="number" id="min_price" name="min_price"
                                                value="<?php echo esc_attr( $current_min_price ); ?>"
                                                data-min="<?php echo esc_attr( $min_price ); ?>"
                                                placeholder="<?php echo esc_attr__( '', 'woocommerce' ); ?>" />
                                            <label for="max_pricee">Até:</label>
                                            <input type="number" id="max_price" name="max_price"
                                                value="<?php echo esc_attr( $current_max_price ); ?>"
                                                data-max="<?php echo esc_attr( $max_price ); ?>"
                                                placeholder="<?php echo esc_attr__( '', 'woocommerce' ); ?>" />
                                            <?php /* translators: Filter: verb "to filter" */ ?>
                                            <button type="submit"
                                                class="button hidden"><?php echo esc_html__( 'Filter', 'woocommerce' ); ?></button>
                                            <div class="price_label" style="display:none;">
                                                <?php echo esc_html__( 'Price:', 'woocommerce' ); ?> <span
                                                    class="from"></span>
                                                &mdash; <span class="to"></span>
                                            </div>
                                            <?php echo wc_query_string_form_fields( null, array( 'min_price', 'max_price', 'paged' ), '', true ); ?>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </form>
                                <?php do_action( 'woocommerce_widget_price_filter_end', $args ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-secao-produtos">
        <div>
        <?php 
            if ( woocommerce_product_loop() ) {

            /**
             * Hook: woocommerce_before_shop_loop.
             *
             * @hooked woocommerce_output_all_notices - 10
             * @hooked woocommerce_result_count - 20
             * @hooked woocommerce_catalog_ordering - 30
             */
            //do_action( 'woocommerce_before_shop_loop' );

            woocommerce_product_loop_start();

            if ( wc_get_loop_prop( 'total' ) ) {
                while ( have_posts() ) {
                    the_post();

                    /**
                     * Hook: woocommerce_shop_loop.
                     */
                    do_action( 'woocommerce_shop_loop' );

                    wc_get_template_part( 'content', 'product' );
                }
            }

            woocommerce_product_loop_end();

            /**
             * Hook: woocommerce_after_shop_loop.
             *
             * @hooked woocommerce_pagination - 10
             */
            //do_action( 'woocommerce_after_shop_loop' );
            } else {
            /**
             * Hook: woocommerce_no_products_found.
             *
             * @hooked wc_no_products_found - 10
             */
            do_action( 'woocommerce_no_products_found' );
            }

            /**
             * Hook: woocommerce_after_main_content.
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
            do_action( 'woocommerce_after_main_content' );

            /**
             * Hook: woocommerce_sidebar.
             *
             * @hooked woocommerce_get_sidebar - 10
             */
            //do_action( 'woocommerce_sidebar' );
        ?>
            </a>
        </div>
    </div>
</div>

</div>

<?php get_footer(); ?>