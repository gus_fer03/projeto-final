<?php
    // Template Name: Home
?>

<?php get_header(); ?>

<div class="home-section-1">
    <div>
        <h1><?php the_field("home-titulo"); ?></h1>
        <p><?php the_field("slogan"); ?></p>
    </div>
</div>

<div class="home-section-2">
    <div class="container-titulo-secao-2"><h2>CONHEÇA NOSSA LOJA</h2></div>
    <div class="container-secao-2-items">
        <div class="container-secao-2-categorias">
            <h3>Tipos de pratos principais</h3>
            <ul class="secao-2-lista">
                <?php            
                    $taxonomy     = 'product_cat';
                    $orderby      = 'name';  
                    $show_count   = 0;      // 1 for yes, 0 for no
                    $pad_counts   = 0;      // 1 for yes, 0 for no
                    $hierarchical = 1;      // 1 for yes, 0 for no  
                    $title        = '';  
                    $empty        = 0;

                    $args = array(
                            'taxonomy'     => $taxonomy,
                            'orderby'      => $orderby,
                            'show_count'   => $show_count,
                            'pad_counts'   => $pad_counts,
                            'hierarchical' => $hierarchical,
                            'title_li'     => $title,
                            'hide_empty'   => $empty
                    );
                    $all_categories = get_categories( $args );
                    foreach ($all_categories as $cat) {
                        if($cat->category_parent == 0) {
                            $category_id = $cat->term_id; 
                            $category_name = $cat->name;
                            $thumbnail_id = get_woocommerce_term_meta($category_id, 'thumbnail_id', true);
                            $image = wp_get_attachment_url($thumbnail_id);
                            $link = get_term_link( $category_id, `product_cat` );

                            if($cat->slug != "uncategorized"){
                                echo "<li>";
                                echo    "<a href='{$link}'><div class='container-secao-2-item-template' style=" . "background-image:url('{$image}');" . ">";
                                echo        "<div><p class='categoria-label'>${category_name}</p></div>";
                                echo        "</div></a>";
                                echo "</li>";
                            }
                        }       
                    } 
                ?>
            </ul>
        </div>
        <div class="container-secao-2-pratos">
            <div class="container-secao-2-titulo">
                <h3>Pratos do dia de hoje:</h3>
                <p><?php
                    setlocale( LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese' );
                    date_default_timezone_set( 'America/Sao_Paulo' );
                    $weekday = strftime( ucwords( "%w",time() ) ); ;
                    switch ($weekday) {
                        case 0:
                            $diasemana = "domingo";
                            echo "DOMINGO";
                            break;
                        case 1:
                            $diasemana = "segunda";
                            echo "SEGUNDA";
                            break;
                        case 2:
                            $diasemana = "terca";
                            echo "TERÇA";
                            break;
                        case 3:
                            $diasemana = "quarta";
                            echo "QUARTA";
                            break;
                        case 4:
                            $diasemana = "quinta";
                            echo "QUINTA";
                            break;
                        case 5:
                            $diasemana = "sexta";
                            echo "SEXTA";
                            break;
                        case 6:
                            $diasemana = "sabado";
                            echo "SÁBADO";
                            break;}
                ?></p>
            </div>
            <ul class="secao-2-lista">
                <?php
                extract(shortcode_atts(array(
                    "tags" => ''
                ), $atts));

                ob_start();

                // Define Query Arguments
                $args = array( 
                            'post_type' 	 => 'product', 
                            'posts_per_page' => -1, 
                            'product_tag' 	 => $tags 
                            );

                // Create the new query
                $loop = new WP_Query( $args );

                // Get products number
                $product_count = $loop->post_count;

                // If results
                if( $product_count > 0 ) :
                        while ( $loop->have_posts() ) : $loop->the_post(); global $product;

                            $terms = get_the_terms( get_the_ID(), 'product_tag' );
                            $term_array = array();
                            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                                foreach ( $terms as $term ) {
                                    $term_array[] = $term->slug;
                                }
                            }
                            if ($term_array[0] == $diasemana){
                                $product_id = $product->id;
                                $thumbnail_id = get_woocommerce_term_meta($product_id, 'thumbnail_id', true);
                                $link = $product->get_permalink();
                                $image_id  = $product->get_image_id();
                                $image = wp_get_attachment_image_url( $image_id, 'full' );

                                echo "<li>";
                                echo    "<a href={$link}>";
                                echo    "<div class='container-secao-2-item-template produto' style='background-image: url(" . $image . ");'>";
                                echo        "<div class='categoria-label'>";
                                echo            "<p>{$product->name}</p>";
                                echo            "<div class='container-preco-icon'>";
                                echo                "<p>R$ {$product->price}</p>";
                                echo                "<button onclick='window.location = `?add-to-cart={$product_id}`'><img src='" . get_stylesheet_directory_uri() . "/img/icone-buy.png'></button>";
                                echo            "</div>";
                                echo        "</div>";
                                echo    "</div>";
                                echo    "</a>";
                                echo "</li>";
                            }
                        endwhile;
                endif; // endif $product_count > 0
                ?>
            </ul>
        </div>
    </div>
    <div class="container-botao-opcoes"><a href="/loja/"><button class="botao-opcoes">Veja outras opções</button><a></div>
</div>

<div class="home-section-3">
    <div class="container-titulo-secao-3"><h2><?php the_field("titulo-secao-3"); ?></h2></div>
    <div class="container-secao-3-items">
        <div class="container-mapa">
            <iframe class="iframe-mapa" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d918.7966950868779!2d-43.1331215!3d-22.9064806!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817e444e692b%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1634087100813!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <div class="container-mapa-texto">
                <div><img class="logo-contato" src="<?php echo get_stylesheet_directory_uri() ?>/img/icone-talher.png"><p>Rua lorem ipsum, 123, LI, Brasil</p></div>
                <div><img class="logo-contato" src="<?php echo get_stylesheet_directory_uri() ?>/img/icone-telefone.png"><p>(XX) XXXX-XXXX</p></div>
            </div>
        </div>
        <div class="container-carrossel"></div>
    </div>
</div>

<?php get_footer(); ?>